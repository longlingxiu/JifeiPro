//
//  JIfeiTools.cpp
//  TestJIfei
//
//  Created by cesc on 14-12-2.
//
//

#include "JIfeiTools.h"

namespace JIFEI_TOOLS {
    
    JIfeiTools* JIfeiTools::i_ = nullptr;
    
    JIfeiTools:: JIfeiTools():
    m_from( "" ),
    i_buyID( -1 )
    {
        
        
    }
    
    JIfeiTools::~JIfeiTools()
    {
        
        
    }
    
    JIfeiTools* JIfeiTools::getInstance()
    {
        if ( i_ == nullptr ) {
            i_ = new JIfeiTools;
        }
        
        return i_;
    }
    
    // 开始计费
    void JIfeiTools::send_jifei( std::string& from , int buID )
    {
        m_from = from;
        
        i_buyID = buID;
        
        // TODO: 调用计费逻辑
        
        
    }
    
    void JIfeiTools::jifei_callback()
    {
        
        JIFEI_DATA data;
        data.isSuccess = true;
        data.itemID = i_buyID;
        
        Director::getInstance()->getEventDispatcher()->dispatchCustomEvent( m_from, (void*)(&data) );
        
    }
    
    // 发送计费
    void sendJifei( std::string& from, int buyID )
    {
        JIfeiTools::getInstance()->send_jifei( from , buyID );
        
        
    }
    
    
    // 计费成功调用
    void jifeiCallback()
    {
        JIfeiTools::getInstance()->jifei_callback();
        
    }
    

    
}

extern "C"{

    void Java_com_cn_cesc_AppActivity_callback( /*参数*/ ){
        
        JIFEI_TOOLS::jifeiCallback();
    }


};