//
//  JIfeiTools.h
//  TestJIfei
//
//  Created by cesc on 14-12-2.
//
//

#ifndef __TestJIfei__JIfeiTools__
#define __TestJIfei__JIfeiTools__

#include "cocos2d.h"
USING_NS_CC;

#include <string>



namespace JIFEI_TOOLS {
    
struct JIFEI_DATA  {
    
    bool isSuccess;
    
    int itemID;
    
};
class JIfeiTools  {
    
public:
    
    JIfeiTools();
    
    ~JIfeiTools();
    
    static JIfeiTools* getInstance();
    
    void send_jifei( std::string& from , int buID );
    
    void jifei_callback();
    
private:
    
    static JIfeiTools* i_;
    
    std::string m_from;
    
    int i_buyID;
    
};
    

    // 发送计费
    void sendJifei( std::string& from, int buyID );
    
    
    // 计费成功调用
    void jifeiCallback();
    

}

#endif /* defined(__TestJIfei__JIfeiTools__) */
